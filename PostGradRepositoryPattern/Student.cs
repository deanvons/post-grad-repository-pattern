﻿namespace PostGradRepositoryPattern
{
    class Student
    {
        
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int ProfID { get; set; }

    }
}