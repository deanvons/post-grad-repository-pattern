﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostGradRepositoryPattern
{
    class SqlClientProfessorHelper 
    {
        public static string GetConnectionString()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = @"NB-DEAN\SQLEXPRESS";
            builder.InitialCatalog = "PostGradDB";
            builder.IntegratedSecurity = true;
            builder.TrustServerCertificate = true;

            return builder.ConnectionString;
        }

        public static bool CreateProfessor(Professor prof)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(GetConnectionString()))
                {

                    connection.Open();

                    string sql = "INSERT INTO Professor (First_Name,Last_Name) VALUES (@FirstName,@LastName)";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {

                        command.Parameters.AddWithValue("@FirstName", prof.FirstName);
                        command.Parameters.AddWithValue("@LastName", prof.LastName);

                        command.ExecuteNonQuery();

                    }

                }

            }
            catch (SqlException)
            {
                Console.WriteLine("Something went wrong in the db");
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something went wrong");
                return false;
            }

            return true;
        }

        public static List<Professor> ReadAllProfessors()
        {
            List<Professor> professors = new List<Professor>();
            try
            {
                using (SqlConnection connection = new SqlConnection(GetConnectionString()))
                {
                    connection.Open();
                    string sql = "SELECT * FROM Professor";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                professors.Add(new Professor() {ID = reader.GetInt32(0), FirstName = reader.GetString(1), LastName = reader.GetString(2) });

                            }
                        }
                    }
                }

            }
            catch (SqlException)
            {
                Console.WriteLine("Something went wrong in the db");

            }
            catch (Exception ex)
            {
                Console.WriteLine("Something went wrong");

            }


            return professors;

        }

        public static void DeleteProfessor(int id)
        {
            string sql = "DELETE FROM Professor WHERE ID = @professorID";

            using (SqlConnection connection = new SqlConnection(GetConnectionString()))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.Parameters.AddWithValue("@professorID", id);

                    command.ExecuteNonQuery();
                }
            }
        }

        public static void UpdateProfessor(Professor prof, int id)
        {
            string sql = "UPDATE Professor SET Last_Name = @newLastName WHERE ID = @professorID";

            using (SqlConnection connection = new SqlConnection(GetConnectionString()))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.Parameters.AddWithValue("@newLastName", prof.LastName);
                    command.Parameters.AddWithValue("@professorID", id);

                    command.ExecuteNonQuery();
                }
            }
        }

    }
}
