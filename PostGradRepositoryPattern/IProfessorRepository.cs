﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostGradRepositoryPattern
{
    interface IProfessorRepository
    {
        bool CreateProfessor(Professor prof);

        List<Professor> ReadAllProfessors();

        void DeleteProfessor(int id);

        void UpdateProfessor(Professor prof, int id);

    }
}
