﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostGradRepositoryPattern
{
    interface IRepository<T>
    {
        
        bool Create(T entity);

        List<T> ReadAll();

        void Delete(int id);

        void Update(T prof, int id);
    }
}
